
from __future__ import absolute_import
from celery import Celery

app = Celery('my',
             broker='amqp://celry:celry123@localhost:5672/myvhost',
             backend='rpc://',
             include=['my.tasks'])